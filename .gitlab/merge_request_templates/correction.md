# Mise à jour d'un paramètre législatif

## Vérifications à effectuer par le contributeur

- [ ] Renseigner la date de parution au JO
- [ ] Renseigner la référence législative
<!-- - [ ] Renseigner les changements portés par la merge request dans [ce tableau](https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-yaml/-/blob/master/doc/recensement_actualisations.md) (Voir documentation, partie ["Etape 5" : Noter mon travail dans le tableau recensant les actualisations des barèmes](https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-yaml/-/blob/master/doc/guide_edition.md#5-noter-mon-travail-dans-le-tableau-recensant-les%20actualisations-des-bar%C3%A8mes)) -->
- [ ] Vérifier que les pipelines passent bien. En cas d'échec, et si le message d'échec est peu clair, s'assurer que les textes (notes, documentation...) incluant des `:` sont mis entre guillemets. ([Voir documentation, partie "My pipeline has failed"](/doc/guide_edition.md))
- [ ] Ne pas oublier de vérifier que le site web est fonctionnel en regardant le site qui a été déployé (lien juste sous le pipeline).

## Vérifications à effectuer par le relecteur

- [ ] Vérifier la date de parution au JO
- [ ] Vérifier la référence législative
- [ ] Ne pas oublier de vérifier que le site web est fonctionnel (lien juste sous le pipeline).
