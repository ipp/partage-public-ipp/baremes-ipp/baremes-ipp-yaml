# Barèmes IPP

Original IPP's tax and benefit tables.

IPP is the [Institut des politiques publiques](http://www.ipp.eu/en/)

Published tax and benefit tables in [English](http://www.ipp.eu/en/tools/ipp-tax-and-benefit-tables) and [French](http://www.ipp.eu/fr/outils/baremes-ipp/)

## Data License

[Licence ouverte / Open Licence](http://www.etalab.gouv.fr/licence-ouverte-open-licence)

## Contirbution

[How to contribute (in french)](https://baremes-ipp-doc.tax-benefit.org/)
