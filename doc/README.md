# Guide de contribution

Les pages des `barèmes législatifs des politiques publiques` rassemblent de nombreux paramètres de la législation socio-fiscale française.
Ceux-ci sont diffusés sous la [Licence ouverte](http://www.etalab.gouv.fr/licence-ouverte-open-licence)

Ils ont bénéficié du soutien du Groupement d'Intérêt Scientifique CollEx-Persée dans le cadre du projet [CoBaLPP](./collex.md).

La contribution aux barèmes législatifs des politiques publiques est ouverte à tout le monde.
Avant d'être intégrée, elle est revue et validée.
Le contributeur doit cependant suivre les règles de contribution. Il doit notamment fournir les références législatives nécessaires à la validation de ces demandes d'ajout ou de correction.

Ce guide documente la contribution aux barèmes législatifs des politiques publiques. Pour en savoir plus, consulter les sections suivantes de ce guide.
