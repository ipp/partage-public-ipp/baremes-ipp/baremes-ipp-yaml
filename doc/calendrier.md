# Calendrier indicatif des publications de modifications des paramètres de la législations socio-fiscale

## Décembre N-1

### Loi de Finances

Concernant l'impôt sur le revenu :

- [ ] [Barème de l'impôt sur le revenu](../parameters/impot_revenu)
- [ ] [Plafond du Quotient Familial et paramètres associés](../parameters/impot_revenu/calcul_impot_revenu/plaf_qf)
- [ ] [Montant maximal de l'abattement pour rattachement d'enfants mariés](../parameters/impot_revenu/calcul_revenus_imposables/abat_rni)

#### Décret portant relèvement du salaire minimum de croissance

- [ ] [Montant du SMIC](../parameters/marche_travail/salaire_minimum/smic)

#### Circulaire CNAV

Cette circulaire est liée au décret revalorisant le SMIC, elle explicite les conséquences de cette revalorisation sur les paramètres de la législation vieillesse.

- [ ] [Salaire forfaitaire de l'AVPF](../parameters/prestations_sociales/solidarite_insertion/minimum_vieillesse/avpf)
- [ ] Abattement forfaitaire au titre des revenus professionnels pour le calcul de l'ASPA

#### Circulaire DSS/SD2B portant revalorisation des plafonds de ressources d’attribution de certaines prestations familiales

Concernant les prestations sociales :

- [ ] [Plafonds de ressources pour bénéficier des Allocations Familiales](../parameters/prestations_sociales/prestations_familiales/prestations_generales/af/af_cond_ress)
- [ ] [Plafonds de ressources pour les majorations du Complément Familial](../parameters/prestations_sociales/prestations_familiales/prestations_generales/cf/cf_plaf)
- [ ] [Plafonds de ressources pour bénéficier de la Paje](../parameters/prestations_sociales/prestations_familiales/petite_enfance/paje/paje_plaf)
- [ ] [Plafonds de ressources pour bénéficier de la Paje-CMG](../parameters/prestations_sociales/prestations_familiales/petite_enfance/paje/plaf_cmg)
- [ ] [Plafonds de ressources pour bénéficier de l'ARS](../parameters/prestations_sociales/prestations_familiales/education_presence_parentale/ars/ars_plaf)

## Janvier

#### Réunion du conseil d'administration de l'[AGS](http://www.ags-garantie-salaires.org/archives.html) généralement doublée d'une [circulaire Unedic](http://www.unedic.org/article/circulaires-de-l-unedic):

- [ ] prélévements sociaux: AGS

#### Circulaire [AGIRC-ARRCO](http://www.agirc-arrco.fr/documentation-multimedia/circulaires-2015/)

- [ ] prélévements sociaux: ARRCO, AGIRC, AGFF, GMP, CET, APEC

## Avril

### Divers décrets et circulaires revalorisant les prestations sociales au 1er avril

Ex : *Circulaire interministérielle DSS/S2B relative à la revalorisation des prestations familiales, Circulaire CNAV*

Concernant les prestations familiales :

- [ ] [Base Mensuelle de Calcul des Allocations Familiales (BMAF)](../parameters/prestations_sociales/prestations_familiales/bmaf)

Concernant les prestations de solidarité :

- [ ] [Montant de base de l'AAH](../parameters/prestations_sociales/prestations_etat_de_sante/invalidite/aah)
- [ ] [Montant et plafonds de ressources de l'AVTS](../parameters/prestations_sociales/solidarite_insertion/minimum_vieillesse/avts)
- [ ] [Montant et plafonds de ressources de l'AS](../parameters/parameters/prestations_sociales/solidarite_insertion/minimum_vieillesse/as)
- [ ] [Montant et plafonds de ressources de l'ASPA](../parameters/prestations_sociales/solidarite_insertion/minimum_vieillesse/aspa)
- [ ] [Montant et plafonds de ressources de l'ASI](../parameters/prestations_sociales/prestations_etat_de_sante/invalidite/asi)

#### Divers décrets et circulaires revalorisant les allocation chômage au 1er avril

- [ ] [Allocation chômage de solidarité](../parameters/chomage/allocations_chomage_solidarite/allocation_aide_publique)

## Mai-Juin

### Décret portant incorporation au code général des impôts de divers textes modifiant et complétant certaines dispositions de ce code

Concernant l'impôt sur le revenu :

- [ ] [Montant maximal et minimal de l'abattement pour frais professionnel](../parameters/impot_revenu/calcul_revenus_imposables/deductions)
- [ ] [Plafond des déficits agricoles imputables sur le revenus imposables](../parameters/impot_revenu/calcul_revenus_imposables/rpns/micro/imputation_sur_le_revenu_global)
- [ ] [Plafond des frais d'acceuil de personne âgée déductibles](../parameters/impot_revenu/calcul_revenus_imposables/charges_deductibles/accueil_personne_agee/)
<!-- - [ ] [Paramètres relatifs à l'abattement pour personnes âgées de plus de 65 ans ou invalide](../parameters/impot_revenu/calcul_revenus_imposables/charges_deductibles/) -->
- [ ] [Plafond de la réduction pour dons "Coluche"](../parameters/impot_revenu/calcul_reductions_impots/dons/dons_coluche)

#### Divers décrets

Concernant les prestations liées au chômage :

- [ ] [Allocation de solidarité spécifique](../parameters/chomage/allocations_assurance_chomage/ass)
- [ ] [Allocation équivalent retraite](../parameters/chomage/preretraites/aer) (effet au 1er avril)

## Juillet

### Réunion du conseil d'administration de l'[AGS](http://www.ags-garantie-salaires.org/archives.html)

- [ ] Prélévements sociaux: AGS

### [Circulaire Unedic](http://www.unedic.org/article/circulaires-de-l-unedic):

- [ ] [Allocation de base d'assurance chômage : partie fixe et minimum](../parameters/chomage/allocations_assurance_chomage/alloc_base)
- [ ] [Salaire de référence des allocations chômage](../parameters/chomage/allocations_assurance_chomage/sr_alloc/sr_alloc.yaml)
