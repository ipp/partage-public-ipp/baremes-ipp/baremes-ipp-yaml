# Le projet CoBaLPP

Le projet [CoBaLPP - Collecte des barèmes législatifs des politiques publiques](https://www.collexpersee.eu/projet/cobalpp/) a été retenu dans le cadre du troisième appel à projets collaboratifs lancé par le GIS CollEx-Persée le 15 juin 2021 et clos le 10 décembre 2021.

Il est porté par PSE - Ecole d'économie de Paris et co-piloté par Colette CADIOU (DipSO - INRAE) et Mahdi BEN JELLOUL (Institut des politiques publiques, PSE - Ecole d'économie de Paris), responsables scientifiques au nom du coordinateur administratif.

Ce projet de collecte cherche à répondre à un besoin des chercheurs et de certaines administrations publiques de disposer d'une source de référence facilement mobilisable. Il vise ainsi à compléter la base de données des barèmes législatifs des politiques publiques initiée par l'[Institut des politiques publiques](https://www.ipp.eu).

D'une part, un effort important de saisie a été entrepris pour améliorer la couverture temporelle des données et améliorer les métadonnées, notamment compléter et fiabiliser les références législatives.

D'autre part, des structures permettant la maintenance, la contribution collaborative, et la réutilisation des données seront mis en place, afin de favoriser la diffusion, l'accessibilité et la fiabilité des données.
Le développement d'un outil de validation permettra d'établir un diagnostic clair de l'existant pour mieux le compléter.
L'introduction d'une interface dédiée permettra d'améliorer la quantité et la qualité des contributions actuelles mais aussi de mobiliser des experts.

Afin de faciliter la découvrabilité des données, les séries de paramètres seront indexées et le corpus est interrogeable à l'aide de son intégration dans un portail [DBnomics](https://db.nomics.world) qui dispose de capacités de visualisation et d'une interface de programmation (API) permettant le téléchargement des séries par les logiciels usuels de statistiques.

La base de données comme les outils développés sont diffusés sous une licence libre ouverte et sont librement réutilisables.
