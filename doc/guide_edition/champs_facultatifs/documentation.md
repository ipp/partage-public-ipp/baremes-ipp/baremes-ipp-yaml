# La documentation (`documentation`)

## Définition

Ce champ de texte libre contient des informations relatives au paramètre, notamment des explications générales, l'extension des acronymes, ou encore des références législatives plus larges que des valeurs spécifiques (par exemple *Service-Public* ou *l'Urssaf*).

Ce champ est **optionnel** et vise à accueillir tous les #commentaires qui seront bientôt supprimés.

## Résumé

- **Nom** : `documentation`
- **Type** : Texte libre
- **Échelle de déclaration** : Racine du paramètre
- **Cas d'usage** :
  - Indiquer les limites ou les choix faits pour la modélisation.
  - Ajouter des information complémentaires.
- **Exemples** :
  - ```yaml
    documentation: |
      À partir de 2014, des seuils spécifiques (célibataire, couple) sont employés pour le calcul de la décote.
  - ```yaml
    documentation: |
      La taxe d'apprentissage est créée par la Loi de finances du 13 juillet 1925. La loi 77-704 du 05/07/1977créé une cotisation supplémentaire de 0,2% pour financer la formation en alternance; elle doit être versée de manière exceptionnelle en 1977. Elle a été maintenue par la LFR pour 1978. En 1990, la contribution supplémentaire de 0,10% est remplacée par une cotisation pérenne de 0,10% pour la formation en alternance.
    ```
  - ```yaml
    documentation: |
      Une incertitude subsiste sur le taux applicable en Alsace-Lorraine entre 1973 et 1978 inclus. Taxe d'apprentissage : CGI, art. 224 et s. et art. 140 A et s. de l'annexe I ; pour l'Alsace-Moselle, art. 1599 ter J.
    ```
