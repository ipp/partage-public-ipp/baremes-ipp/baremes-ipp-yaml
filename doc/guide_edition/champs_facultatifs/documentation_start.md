# Signaler l'origine de la documentation contextuelle (`documentation_start`)

## Définition

Ce champ est booléen. Il vise à signaler le noeud le plus haut auquel on doit remonter pour construire une documentation pertinente en agrégeant les champs `documentation` intermédiaires.

Ce champ est **optionnel**. Sa valeur pa défaut est `false`.

## Résumé

- **Nom** : `documentation_start`
- **Type** : true ou false
- **Échelle de déclaration** : Noeud le plus haut à partir duquel seront agrégés les éléments de documentation
- **Cas d'usage** :
  - Construire une documentation complète pour un noeud en agrégeant les documentations contextuelles de niveau supérieur pour créer une documentation complète et indépendante.
- **Exemples** :
```diff
   metadata: 
+    documentation_start: true
```
