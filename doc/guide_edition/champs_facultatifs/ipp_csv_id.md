# L'identifiant IPP (`ipp_csv_id`)

Le champ `ipp_csv_id` est directement issu de l'harmonisation avec l'IPP, et sert à la génération de leur site.

Ce champ est **optionnel** et **sera retiré** à l'issue du chantier d'harmonisation avec les barèmes de l'IPP.
