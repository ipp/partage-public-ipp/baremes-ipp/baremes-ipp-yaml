# La description longue en anglais (`label_en`)

Le champ `label_en` est est affiché sur la version anglaise du site des Barèmes Ipp. Il s'agit de la traduction en anglais du champ `label`.

Ce champ est **optionnel**.
