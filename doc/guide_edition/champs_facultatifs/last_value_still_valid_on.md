# La date de validité de la dernière valeur en cours la plus récemment vérifiée (`last_value_still_valid_on`)

## Définition

Ce champ permet de témoigner que la **dernière valeur** (`value`) d'un paramètre est bien toujours en vigueur à la date `YYYY/MM/DD` (Date de la relecture).

Comme indiqué par son nom, cette métadonnée **ne concerne pas toutes les `value`**. Elle atteste uniquement que la dernière valeur a été vérifiée comme étant toujours en vigueur. Ainsi, la date de ce champ est forcément postérieure ou égale à la dernière valeur du paramètre (_et ceci peut facilement être vérifié automatiquement_).

En pratique, il est impératif de fournir la référence législative à date affichant directement la **dernière valeur** à jour. Si cet `href` est une information nouvelle (par exemple, changement d'emplacement de l'article dans le code), le contributeur peut décider de l’ajouter dans les références au niveau de la dernière date de `value`.
On mettra l'`href` à date uniquement dans la merge request dans le cas où il est déjà dans les références, afin d'éviter les redondances.

Ce champ est **optionnel** et permet ainsi la capitalisation progressive de cette information dans la base de paramètres.

## Résumé

- **Nom** : `last_value_still_valid_on`
- **Type** : String contenant une date
- **Échelle de déclaration** : Champ `metadata`
- **Cas d'usage** :
  - Indiquer qu’un paramètre est à jour, même si sa valeur n’a pas changé depuis X années.
  - Indiquer en front la dernière date de validité du code
- **Exemple** :
    ```diff
    values :
    2020-04-01:
        value: 200
        reference:
        title: Décret n° 2020-769 du 24/06/2020, art. 2
        href: https://www.legifrance.gouv.fr/loda/id/JORFTEXT000042032514
    metadata:
    +  last_value_still_valid_on: "2023-01-24"
    ```
