# Les notes (`notes`)

## Définition

Le champ `notes` contient des informations spécifiques à une date.
Il est donc spécifique aux paramètres.

Ce champ est **optionnel**.

## Résumé

- **Nom** : `notes`
- **Type** : Texte libre
- **Échelle de déclaration** : Champ `metadata`
- **Cas d'usage** :
  - Ajouter une information spécifique à une `value` ou une année
  - Ajouter une note en bas de chaque fichier Excel des barèmes IPP.
- **Exemples** :
    ```yaml
    values:
        2021-01-01:
        value: 200
    metadata:
      notes:
        2021-01-01: Ce montant ne concerne plus les retraités
    ```
