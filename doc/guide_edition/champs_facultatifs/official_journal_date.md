# La date de publication au journal officiel (`official_journal_date`)

## Définition

Il se compose des dates de publication au Journal Officiel, pour chaque `value`. S’il y a plusieurs publications par date, on fait un tableau `[2019-01-01, 2019-01-04]`.

Ce champ est **optionnel**.

## Résumé

- **Nom** : `official_journal_date`
- **Échelle de déclaration** : Champ `metadata`
- **Cas d'usage** :
  -  Pouvoir retrouver la publication dans le journal officiel
- **Exemples** :
    ```diff
    values:
    2021-01-01 :
        value: 200
        reference:
        title: Décret n°2020-769 du 24/06/2020, art. 2
        href: https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000042032526/
    2018-10-01 :
        value: 190
        reference:
        title: Décret n°2020-743 du 10/06/2018
        href: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042032514
    metadata:
    +  official_journal_date :
    +     2021-01-01: "2020-06-24"
    +     2018-10-01: [2018-06-12, 2018-06-10]
    ```
