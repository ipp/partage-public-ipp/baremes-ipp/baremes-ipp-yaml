# La description courte (`short_label`)

## Définition

La métadonnée `short_label` répond au besoin de désigner un noeud ou un paramètre de la façon la plus signifiante et la plus courte possible.

C'est donc un nom non-ambigu avec un nombre limité de caractères. Par convention, le nombre maximum est fixé à 70, mais le plus concis est le mieux.
Ce nom court autorise les abréviations les plus connues.
Il s'inscrit toujours dans un contexte de présentation reprenant l'arborescence du yaml, permettant ainsi de différencier deux paramètres ayant le même `short_label`.
Il peut servir dans des interfaces graphiques (sites, tableaux, graphiques, etc.)

Cette métadonnée est aussi introduite dans les `index.yaml` de noeuds (cf. section ci-dessous).

Ce champ est **optionnel**.


## Résumé

- **Nom** : `short_label`
- **Type** : langage naturel limité à 70 caractères, sur une seule ligne, sans espace en début ni fin.
- **Échelle de déclaration** : Champ `metadata`
- **Cas d'usage** :
  - Afficher un nom reconnaissable par une personne non experte du modèle OpenFisca dans un espace d'affichage connu d'avance.
  - Faciliter la construction d'interfaces graphiques en ayant une contrainte connue de taille d'affichage.
- **Exemples** :
  - `Taux réduit`
  - `Fonds national d'aide au logement (FNAL)`
  - ```diff
    description: Abattement de la contribution Sociale Généralisée (CSG) déductible sur les revenus d'activité
    [...]
    metadata:
    +  short_label: Abattement
    ```

## Guide des bonnes pratiques

### Éviter les confusions

Dans certains cas, le `short_label`, même présenté dans son contexte d'arborescence, peut prêter à confusion.

**Exemple:**
Pour le paramètre `parameters.prelevements_sociaux.contributions_sociales.csg.chomage.imposable.taux_reduit.yaml`, l'arborescence des `short_label` (dans les `index.yaml` des noeuds, puis dans le paramètre final) devrait être le chemin suivant:

> `Prélèvements sociaux / Contributions de Sécurité sociale du régime général / CSG / Chômage / Imposable / Taux réduit`

Cependant, on va s'autoriser un peu de redondance par souci de clarification:

> `Prélèvements sociaux / Contributions de Sécurité sociale du régime général / CSG / **Allocations** chômage / **CSG** imposable / Taux réduit`

**Ceci permet de souligner deux choses :**

- La CSG est prélevée *sur les allocations chômage* et non pas *pour l'assurance chômage*

- Il s'agit de la part *imposable de CSG* et non pas d'une CSG prélevée *sur la part imposable des allocations chômage*

### Utiliser le nom en toute lettre quand il est court

Comme explicité, le short_label a pour objectif d'être le plus court possible tout en étant le plus signifiant.

Dans le cas du nom d'un dispositif (cas qui intervient pour les noeuds de paramètres.), si le nom du dispositif est court, il vaut mieux le privilégier à l'acronyme :

**Exemple :**
Pour le noeud `/prestations_sociales/solidarite_insertion/minima_sociaux/ppa`, noeud qui regroupe les paramètres de la prime d'activité. On choisira pour short_label : `Prime d'activité`, nom très court, plutôt que `PPA` qui est moins clair :

  ```diff
  description: Prime d'activité (PA ou PPA)
  metadata:
    short_label: Prime d'activité
  ```
