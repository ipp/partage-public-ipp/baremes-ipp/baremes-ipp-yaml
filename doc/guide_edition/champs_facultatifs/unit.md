# L'unité (`unit`)

## Définition

Cette metadata indique l'unité du paramètre.

Ce champ est **optionnel** et permet ainsi la capitalisation progressive de cette information dans la base de paramètres.

L'ajout d'une unité permet souvent de raccourcir ou expliciter les champs [`label`](../champs_obligatoires/label.md) et [`short_label`](../champs_facultatifs/short_label.md).

> ⚙️ La liste des unités se trouve actuellement dans un [fichier `units.yaml` extérieur à openfisca-france](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-openfisca-json/-/blob/master/units.yaml). S'il se confirme que ce fichier est utile pour plusieurs projets (actuellement [baremes-ipp-views](https://framagit.org/french-tax-and-benefit-tables/baremes-ipp-views/), [baremes-ipp-edit](https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-edit) et [leximpact-socio-fiscal-ui](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-ui)), il sera éventuellement intégré au dépôt openfisca-france.


## Résumé

- **Nom** : `unit`
- **Type** : String
- **Échelle de déclaration** : Champ `metadata`
- **Cas d'usage** :
  - Harmoniser les unités entre paramètres
  - Indiquer en front une unité claire et documentée
- **Exemple** : 
  - ancien franc
  - part de quotient familial
  - ```diff
    metadata:
    +  unit: currency
    ```
## Remarques

Le [fichier `units.yaml` extérieur à openfisca-france](https://git.leximpact.dev/leximpact/leximpact-socio-fiscal-openfisca-json/-/blob/master/units.yaml) offre la possibilité de donner plusieurs label à l'unité.

> Exemple: 
>  ```yaml
>  - name: hour
>    label:
>      one: heure
>      other: heures
>  ```
>
