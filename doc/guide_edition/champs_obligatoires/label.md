# La description longue (`label`)

## Définition

Ce champ **obligatoire** est une description en français du paramètre. Il n'est pas limité en termes de caractères et doit être auto-suffisant, c'est-à-dire que **cette description doit permettre de le distinguer le paramètre de façon unique dans le système socio-fiscal**. Cela passe par le fait de rappeler l'ensemble des éléments de contexte du paramètre, ou *a minima* d'indiquer le dispositif dans lequel le paramètre intervient.


## Résumé

- **Nom** : `label`
- **Type** : Langage naturel non limité, sur une seule ligne, sans espace en début ni fin, unique dans le système socio-fiscal.
- **Échelle de déclaration** : Racine du paramètre
- **Cas d'usage** :
  - Eviter les ambigüités quand le nom d'un paramètre est une abréviation ou très générique.
  - Rechercher et afficher un paramètre dans l'explorateur de législation.
- **Exemples** :
  - `Taux réduit de la contribution sociale généralisée (CSG) imposable sur les allocations chômage`
  - `Montant minimum journalier de l'allocation retour emploi (ARE) hors Mayotte`
  - `Contribution exceptionnelle sur les hauts revenus`


## Guide des bonnes pratiques

### Construction du label
**En théorie et par convention**, la description doit contenir le nom de l'ensemble des nœuds traversés dans l'arborescence depuis le dossier `parameters`.

**En pratique**, il est toléré de raccourcir un peu le `label` s'il est vraiment trop long et que certaines choses sont explicites (par exemple, si l'on parle des "prestations pour la petite enfance", on ne sera pas obligé de répéter "prestations pour la petite enfance parmi les prestations familiales des prestations sociales").

### Préciser la périodicité

Si la périodicité n'est pas triviale, alors il faut la préciser.

> **Par exemple** : Pour un montant/jour, il est probablement intéressant de préciser que le montant est journalier.


### Détailler les abréviations

On demande aussi de donner le nom complet des abréviations en précisant les acronymes entre parenthèse.

> **Par exemple** :
> Contribution sociale généralisée (CSG)
