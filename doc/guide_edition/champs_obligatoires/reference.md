
# Une référence législative ou réglementaire (`reference`)

## Définition

Une référence législative ou réglementaire est **exigée** à chaque ajout ou modification d'une valeur, afin d'identifier la loi d'où provient le paramètre et ainsi permettre la revue des modifications implémentées.

La règle est de fournir une référence où l'on peut identifier la valeur concernée **en un clic**. Autrement dit, **la valeur doit être écrite dans la référence** législative transmise.

Ainsi, cette référence peut être :

- un lien direct vers l'article de loi en vigueur à date ;

- un lien vers le décret qui modifie la valeur (utiliser la [version initiale](#tutoriel-pour-choisir-la-version-initiale-dun-décret) du décret qui contient la valeur) ;

- ou, idéalement, les deux  : l'article en vigueur et le décret à l'origine de la modification.

Il est impératif que la référence soit **officielle** (lien LégiFrance vers un article ou un décret de préférence, circulaire officielle, etc.) et il est préférable de sélectionner un article de loi codifié.

Pour chaque référence, il faut :

- un intitulé `title`

- une URL `href`

Exceptionnellement, il peut être toléré un `title` seul dans le cas où aucune ressource en ligne n'existe (paramètres très anciens).

## Résumé

- **Nom** : `reference`
- **Échelle de déclaration** : Champ `value`
- **Cas d'usage** :
  - Vérifier la validité d'une formule en consultant son origine légale.
  - Afficher l'intitulé de la source légale dans l'explorateur de législation.
  - Automatiser la détection d'un changement législatif.
- **Exemples** :

```yaml
  values:
    2021-01-01:
      value: 1592
      reference:
       - title: Article 197, I.1. du Code général des impôts
         href: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042907517/2022-01-01/
       - title: LOI n° 2021-1900 du 30/12/2021 de finances pour 2022, art. 2, I, b)
         href: https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000044637653
    2022-01-01:
      value: 1678
      reference:
       - title: Article 197, I.1. du Code général des impôts
         href: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042907517/2023-01-01/
       - title: LOI n° 2022-1726 du 30/12/2022 de finances pour 2023, art. 2, I, b)
         href: https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000046845640
  ```

  ```yaml
  values:
    2020-06-01:
      value: 200
      reference:
        title: Décret n°2020-769 du 24/06/2020, art. 2
        href: https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000042032526
  ```

  ```yaml
  values:
    2020-10-01:
      value: 150
      reference:
        title: Décret n°2020-1453 du 27/11/2020, art. 2
        href: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042574431
  ```

## Guide des bonnes pratiques

### Écriture du `title`

Pour le titre `title`, il est utile de suivre les règles suivantes pour harmoniser l'écriture des références :

- Mettre le nom de l’article avant le nom de la loi ou du code qui l’englobe.

- Toujours spécifier le code si l’article est codifié.

- Mettre une majuscule à « Code » et une minuscule pour les mots qui suivent.

  > Exemple :  `Article 197 du Code général des impôts`

- S’il est utile d’ajouter le paragraphe ou le numéro de l’article. Séparer par une virgule :

  > Exemple : `Article 197, 1. du Code général des impôts`

- Si vous souhaitez ajouter le numéro de l’alinéa *(pertinent uniquement si l’article est très grand, à éviter dans les autres cas, car ça alourdit beaucoup la référence)*

  > Exemple :
  >  ```yaml
  >  - Article 197, 1. §5 du Code général des impôts
  >  - Article 197, 1. §§5 à 10 du Code général des impôts
  >  ```

### Écriture de l'URL Légifrance : sans date et sans section

#### Retirer la date de votre consultation dans l'URL

Lorsque vous sélectionnez une URL sur Légifrance, celle-ci contient souvent par défaut la date du jour de la consultation. Cette date ne correspond à rien de légal, retirez-là.

**❌ À éviter :** Une URL avec la date de votre consultation.
`https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044978323/2022-10-31/`

#### Retirer le parcours de recherche et la section

Il faut retirer tout ce qui est après le numéro de l’article, qui correspond au parcours de recherche dans Legrifrance et qui n’apporte pas d'informations.

**✅ À choisir**  :
`https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044978323`

**❌ À éviter** : Voici un exemple d’href qui n’a pas été nettoyée :
`https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000044978323?init=true&nomCode=E92-7A%3D%3D&page=1&query=&searchField=ALL&tab_selection=code`


#### Règles de la syntaxe YAML

Pour que la référence passe les tests et fonctionne, voici quelques éléments de syntaxe à respecter :

##### Ajouter plusieurs références au même paramètre

Pour mettre plusieurs références pour un même paramètre, il faut créer une liste avec des tirets.
Ce [fichier yaml est un bon exemple de référence](https://github.com/openfisca/openfisca-france/blob/5c3d222a8e1b9e076deef2d4471645d5716aeac3/openfisca_france/parameters/prestations_sociales/prestations_familiales/prestations_generales/af/af_maj/duree_majoration_plus_de_20_ans.yaml), où il peut y avoir plusieurs références pour une seule et même date.

**À faire :**
```yaml
values:
  2003-07-01:
    value: 1000
    reference:
      - title: Article 197 du Code général des impôts
        href: https://www.legifrance.gouv.fr/URLdeLaPremiereReference
      - title: Décret 2003-573 du 27/06/2003
        href: https://www.legifrance.gouv.fr/URLdeLaSecondeReference
>
```

**Ne fonctionne pas :** Il faut veiller à garder la syntaxe ci-dessus, y compris l’indentation, sinon les tests ne passent pas. L'exemple ci-dessous ne fonctionnera pas car l'indentation n'a pas été respectée pour les 2 références ci-dessous :
```yaml
values:
  2003-07-01:
    value: 1000
    reference:
    - href: https://www.legifrance.gouv.fr/URLdeLaPremiereReference
    title: Article 197 du Code général des impôts
      - href: https://www.legifrance.gouv.fr/URLdeLaSecondeReference
      title: Décret 2003-573 du 27/06/2003
```

##### Les références sans `href`

Même si cette pratique n'est pas du tout à privilégier, on notera, qu'à l'heure actuelle, il existe de nombreuses références sans `href`. Elles prennent alors la forme suivante:

```yaml
  base:
    values:
      1979-07-01:
        value: 150
  metadata:
    reference:
      1979-07-01:
       - Loi cadre 79-32 du 16/01/1979
       - Arrêté du 02/05/1979 portant agrément de la convention du 27/03/1979
```

> 💡 **Pourquoi il est important de mettre un lien ?**
> Chaque modification dans OpenFisca passe par une relecture par un tiers. Ne pas indiquer l'URL, c'est finalement alourdir le travail de relecture et perdre du temps collectivement.

#### Cas particulier : Ajout d'une référence plus récente

Je suis dans une situation où j'ai besoin d'ajouter une référence dont la date est postérieure à la dernière valeur du paramètre :

1. La référence (article) d'une valeur a changé, mais pas sa valeur.

2. La seule référence en ma possession est postérieure à la dernière valeur.

3. Le dispositif a changé (réforme) mais pas cette valeur.

4. Je trouve une référence récente qui détaille un mécanisme ou une réforme.

Dans les cas 1, 2 et 3, on peut ajouter cette référence (à la date de la `value`) si celle-ci n'est pas redondante avec les références précédentes. (Par redondant, on entend: le même lien LégiFrance mais pas à la même date).

Dans le cas 4, cette référence étant un peu plus large que la `value`, il faudra plutôt l'ajouter dans le champ `documentation`.

On notera qu'il a été décidé de ne pas ajouter de `values` dans le futur (même si on a déjà des décrets publiés, car la loi peut changer d'ici-là et le risque serait de ne pas s'en apercevoir).

### Tutoriel pour choisir la version initiale d'un décret

Dans Légifrance les décrets peuvent se présente sous deux formes :

- sous la forme d'une liste de changements
  > Exemple :
  > https://www.legifrance.gouv.fr/loda/id/LEGITEXT000005629053/2020-11-08/

- sous la forme du nouveau texte, tel que modifié par le décret. C'est cette forme, la "version initiale" qu'il faut sélectionner.
  > Exemple du même décret mais en "version initiale" :
  > https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000000568187

Lorsque l'on cherche un décret par le biais d'un navigateur, il est fréquent que le lien transmis soit celui vers de texte avec liste de changements. Pour obtenir le décret en *version initiale*, il faut cliquer sur le bouton "Version initiale" situé sous le titre du décret sur page Légifrance. ![decret-version-initiale](../../assets/reference-image-bouton-decret-version-initiale.png)
