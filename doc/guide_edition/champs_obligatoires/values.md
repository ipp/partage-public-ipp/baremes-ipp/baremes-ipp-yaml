# Les valeurs (`values` / `brackets`)

## Définition

Le champ `values` contient les valeurs des paramètres à partir de leur date d'application **dans le calcul** ou date d'effet.

En effet, la date associée à un paramètre étant celle appliquée pour les simulations, il s'agit de la date **d'entrée en application du point de vue du calcul du dispositif**. Or, cette date peut être différente de la date de publication du texte ou d'application (elle peut être mentionnée à la fin du texte officiel qui met en place la réforme, dans un décret d'application, etc.)

Dans le cas d'un **barème**, ce champ se nomme `brackets` et contient deux sous unités: un seuil (`threshold`) et une valeur (qui peut être un taux `rate`, un montant `amount`, ...)

Lorsqu'un dispositif est éteint, les values passent à `null`, et non pas à `0` qui indique juste la valeur du paramètre (on a vu certains paramètres passer à zéro pendant une année sans que le dispositif ne soit éteint pour autant).

## Résumé

 - **Nom** : `values` ou `brackets`
 - **Type** : Float
 - **Échelle de déclaration** : Racine du paramètre
 - **Exemple** dans le cas d'un paramètre simple:
 ```yaml
 values:
     1995-01-01:
       value: null
     1994-07-01:
       value: 74.01
     1993-01-01:
       value: 72.92
     1992-07-01:
       value: 71.98
     1992-01-01:
       value: 70.71
 ```
 - **Exemple** dans le cas d'un barème:
 ```yaml
 brackets:
   - rate:
       1967-10-01:
         value: 0.095
       1970-08-01:
         value: 0.1025
       1981-11-01:
         value: 0.0545
       1984-01-01:
         value: null
     threshold:
       1967-10-01:
         value: 0.0
       1984-01-01:
         value: null
   - rate:
       1967-10-01:
         value: 0.02
       1976-01-01:
         value: 0.025
       1981-11-01:
         value: 0.08
       1984-01-01:
         value: 0.126
       1992-01-01:
         value: 0.128
       2016-01-01:
         value: 0.1284
       2018-01-01:
         value: 0.13
     threshold:
       1967-10-01:
         value: 1.0
       1984-01-01:
         value: 0.0
 ```

## Remarques

### Le cas de l'impôt sur le revenu (IR)

Pour l'impôt sur le revenu, il y a une subtilité supplémentaire :

- l'IR est un dispositif annuel, donc sa date d'application pour le calcul de l'impôt ne peut être qu'un 1er janvier.

- la convention actuelle dans Openfisca est en "année revenus", c'est-à-dire que la variable `ir` pour une année N correspond, non pas à l'IR de la déclaration de revenus faite en année N, mais à l'IR au titre des revenus de l'année N (déclaration en N+1).

Donc, **les paramètres de l'IR N+1 sur les revenus N sont à mettre systématiquement au 01/01/N**. Une alternative serait une convention en "année paiement" (l'`ir` de l'année N serait l'impôt effectivement payé en N), mais l'impôt sur les revenus N n'est pas forcément payé en N+1. N+1 n'est que la date de déclaration. Pour le paiement, il y a maintenant le prélèvement à la source et avant, il y avait un système d'acomptes et chaque contribuable pouvait choisir entre différentes options.


> **Exemple** : La loi de finances pour 2023 a changé le seuil de la deuxième tranche de l'impôt sur le revenu : qui est passé de 2 070 euros à 27 478.
>
> En théorie, la `value : 27478"` devrait figurer après la date `2023-01-01`.
>
> Ce n'est pas le cas, comme vous pouvez le voir sur le [fichier .yaml](https://github.com/openfisca/openfisca-france/blob/master/openfisca_france/parameters/impot_revenu/bareme_ir_depuis_1945/bareme.yaml) : la règle ici est d'indiquer la date des revenus qui seront imposés, c'est-à-dire les revenus de 2O22 : `2022-01-01`.
