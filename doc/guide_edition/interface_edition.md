# Interface d'édition

Il existe [une interface conviviale d'édition des paramètres](https://baremes-ipp-edit.tax-benefit.org/) qui permet des les modifier plus facilement qu'en éditant directement les fichiers `YAML`, soit en utilisant les moyens d'édition de la [forge GitLab](https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-yaml) ou en soumettant des modifications locales sur cette dernière via git.

Elle permet également à tout contributeur disposant d'un compte `GitLab` ou `GitHub` de s'identifier de façon à authentifier sa contribution. S'il n'as pas de compte, il peut néanmoins laisser ses coordonnées, notamment son adresse électronique, afin qu'il se signale et qu'une prise de contact soit possible au cas où elle serait utile pour valider la contribution.
