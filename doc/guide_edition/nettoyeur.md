# Le nettoyeur

## Objectif

Afin de pouvoir comparer facilement deux fichiers de données, il est nécessaire qu'ils soient formatés de façon identique.

Il est possible d'activer un nettoyeur des données qui assure qu'un fichier de paramètre soit formaté correctement et en retire les informations inutiles.

Le mise au format consiste notamment en une correction de l'ordre des méta-données et des valeurs (chronologie) et de l'indentation de l'ensemble des éléments.
Les informations inutiles retirées sont les références et notes dont les dates ne correspondent à aucune valeurs.

## Utilisation

### Lors de la soumission

L'activation se fait manuellement dans le pipeline d'une "merge-request" en cours.

### En local

Il est également possible d'activer le nettoyeur en local en procédant comme suit:

```bash
git clone https://git.en-root.org/tax-benefit/openfisca-json-model.git
cd openfisca-json-model/packages/lib/
npm install
npm run build
cd ../tools
npm install
npx tsx src/scripts/clean_raw_unprocessed_parameters_in_place.ts --units=/home/user/projects/openfisca/openfisca-france/openfisca_france/units.yaml ~/project/openfisca/openfisca-france/openfisca_france/parameters/prestations_sociales/aides_logement/
```

Le package évolue peu, donc la plupart du temps la dernière ligne devrait suffire.
