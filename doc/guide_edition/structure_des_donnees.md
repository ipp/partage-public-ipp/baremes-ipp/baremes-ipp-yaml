# La structure des données

Les paramètres de la législation socio-fiscale française forment des séries de données évoluant dans le temps. On présente ici la structuration choisie pour représenter ces données et les règles de contribution associées.

## Le format YAML

Le format [YAML](https://fr.wikipedia.org/wiki/YAML) est le format retenu pour les paramètres législatifs des [barèmes IPP](https://www.ipp.eu/baremes-ipp/) et du moteur [OpenFisca](https://openfisca.org/doc/coding-the-legislation/legislation_parameters.html). Il permet de représenter de façon lisible des informations élaborées comme une combinaison de listes et de dictionnaires imbriqués.

Il est nécessaire de bien respecter :

- l'indentation introduisant les sous-structures

- les tirets `-` marquant des listes

- les deux points séparant les clés des valeurs dans un dictionnaire

Enfin, c'est un schéma bien précis qui est attendu, avec des champs obligatoires et d'autres facultatifs.
Tous ces champs doivent également suivre une structure plus ou moins stricte.

Toute modification est scrutée par un [validateur](./validateur.md) qui détecte les éventuelles erreurs sur les branches suivies.

## Les champs obligatoires

Les paramètres choisis doivent représenter la loi le plus fidèlement possible. Chaque évolution d'une valeur doit être justifiée par une référence législative.

Ainsi, afin de respecter ce principe tout en réduisant au strict minimum la charge pour le contributeur, seuls trois champs sont obligatoires lors de la création d'un paramètre :

- la description longue du paramètre ⎪ champ `label` correspondant à l'ancien champ `description`

- les valeurs du paramètre ⎪ champ `values` (pour les valeurs uniques) ou champ `brackets` (pour les barèmes)

- la référence législative ⎪ champ `reference` qui se situe dans le champ `metadata`

Comme ces champs sont obligatoires, leur présence et leur format seront soumis au validateur automatique.
Par convention, les commentaires (`# commentaires`) sont interdits et seront supprimés automatiquement à la validation.
Il est ainsi recommandé de mettre ses annotations directement dans le champ facultatif [`documentation`](./champs_facultatifs/documentation.md).

Pour savoir comment compléter les champs obligatoires, veuillez consulter les documentations suivantes :

- [La description longue (`label`)](./champs_obligatoires/label.md)

- [Les valeurs (`values` / `brackets`)](./champs_obligatoires/values.md)

- [La référence législative ou réglementaire (`reference`)](./champs_obligatoires/reference.md)

## Les champs facultatifs : `metadata`

Les champs dits facultatifs sont rassemblés dans les métadonnées.
Ils sont dès lors facultatifs au sens d'OpenFisca avec lequel le format est partagé et qui n'impose pas de contrainte dans le champ `metadata`.
Il reste néanmoins fortement conseillé de les renseigner pour la législation socio-fiscale française.

<!-- TODO facultatifs dans quel sens ? -->

OpenFisca traite de différents pays. Les paramètres qui nous intéressent ici concernent la législation française où les champs retenus sont les suivants :

- [La description courte (`short_label`)](./champs_facultatifs/short_label.md)

- [La description longue en anglais (`label_en`)](./champs_facultatifs/label_en.md)

- [La date de validité de la dernière valeur en cours la plus récemment vérifiée (`last_value_still_valid_on`)](./champs_facultatifs/last_value_still_valid_on.md)

- [La date de publication au journal officiel (`official_journal_date`)](./champs_facultatifs/official_journal_date.md)

- [L'unité (`unit`)](./champs_facultatifs/unit.md)

- [Les notes (`notes`)](./champs_facultatifs/notes.md)

- [La documentation (`documentation`)](./champs_facultatifs/documentation.md)

- [Signaler l'origine de la documentation contextuelle (`documentation_start`)](./champs_facultatifs/documentation_start.md)

## Exemple d'un paramètre complet

```yaml
description: Montant (en % de la BMAF) de l'allocation d'adoption (AA), une des prestations sociales familiales pour la petite enfance
values:
  1995-01-01:
    value: 0.3
  1996-08-01:
    value: 0.4595
  2007-01-01:
    value: null
metadata:
  short_label: Montant
  last_value_still_valid_on: "2022-02-22"
  description_en: "Adoption allowance (AA): General conditions and amounts"
  ipp_csv_id: aa_montant
  unit: BMAF
  reference:
    1996-08-01:
    - title: Article 197, I.1. du Code général des impôts
      href: https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000042907517
    - title: Décret n°2020-769 du 24/06/2020, art. 2
      href: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042032514
    2007-01-01:
    - title: Décret n°2020-1453 du 27 novembre 2020
      href: https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000042574431
  official_journal_date:
    1995-01-01: "1995-02-17"
    1996-08-01: "1996-07-06"
    2007-01-01: "2003-12-19"
  notes:
    1995-01-01:
    - title: Pas cumulable avec l'ASF. Pour les enfants nés à partir du 01/01/1995. Le décret 95-165 du 16/02/1995 crée l'Allocation à l'Adoption
    1996-08-01:
    - title: Loi qui assure la parité des droits sociaux attachés à la naissance et à l'adoption. Loi qui aligne l'allocation d'adoption sur l'APJE. Mêmes montants et mêmes plafonds (par contre, durée de l'aide, à fixer par décret).
documentation: |
  Après 2007, la PAJE remplace en partie l'allocation d'adoption. Mais, transition progressive, jusqu'à fin 2006 (cf. art. 60, VIII de la loi).
```

## FAQ - Cas particuliers et questions ouvertes

Selon les paramètres, il arrive de rencontrer des cas un peu particuliers. Voici une liste des décisions prises pour chacun de ces cas. À noter qu'elles n'ont pas fait l'objet d'une RFC (Request For Comment de la communauté de contributeurs), et ne sont donc pas le résultat d'un consensus, mais plutôt de *bonnes pratiques observées*.

### Ajout de références 'récentes'

Est-ce que je peux ajouter une `reference` qui a une date postérieure à la dernière valeur du paramètre ?

Exemples :

1. La référence (article) d'une valeur a changé, mais pas sa valeur.

2. La seule référence en ma possession est postérieure à la dernière valeur.

3. Le dispositif a changé (réforme) mais pas cette valeur.

4. Je trouve une référence récente intéressante qui détaille un mécanisme ou une réforme, mais pas spécifiquement le paramètre.

Dans les cas 1, 2 et 3, on peut ajouter cette référence si celle-ci n'est pas redondante avec les références précédentes (par redondant, on entend par exemple : le même lien LégiFrance mais pas à la même date).

Dans le cas 4, cette référence étant un peu plus large que la `value`, il faudra plutôt l'ajouter dans le champ `documentation`.

### Le `short_label` des paramètres "feuille" peut porter à confusion

Le `short_label` des paramètres au bas de l'arborescence doit être court tout en étant signifiant.

Pour le paramètre [`parameters/prestations_sociales/prestations_familiales/education_presence_parentale/ars/ars_m/taux_primaire.yaml`](../../parameters/prestations_sociales/prestations_familiales/education_presence_parentale/ars/ars_m/taux_primaire.yaml)

Le paramètre noeud `ars_m` a pour `short_label` : "Montants de l'allocation de rentrée scolaire".

En théorie, on pourrait conclure, que le `short_label` du paramètre "feuille" `taux_primaire` devrait être simplement :

> *Première tranche d'âge*

En pratique, on va privilégier la redondance et préférer :

> ***Montant** pour la première tranche d'âge*

Cela permet d'éviter le quiproquo suivant : que le paramètre soit compris comme étant le seuil de la première tranche d'âge.

<!-- TODO lien problématique car non homogénéisé -->

<!-- ### Le `short_label` des paramètres "noeuds" peut porter à confusion

Pour le paramètre [`parameters/prelevements_sociaux/contributions_sociales/csg_remplacement/allocations_chomage/taux_csg_deductible`](../../parameters/prelevements_sociaux/contributions_sociales/csg_remplacement/allocations_chomage/taux_csg_deductible.yaml), l'arborescence des `short_label` (dans les `index.yaml` des noeuds) devrait être le chemin suivant:

> *Prélèvements sociaux / Contributions sociales / CSG / Chômage / Imposable / Taux réduit*

Cependant, on va s'autoriser un peu de redondance par souci de clarification:

> *Prélèvements sociaux / Contributions de Sécurité sociale du régime général / CSG / **Allocations** chômage / **CSG** imposable / Taux réduit*

Ceci permet de souligner deux choses:

- La CSG est prélevée *sur les allocations chômage* et non pas *pour l'assurance chômage*

- Il s'agit de la part *imposable de CSG* et non pas d'une CSG prélevée *sur la part imposable des allocations chômage* -->
