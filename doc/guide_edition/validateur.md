# Le validateur

Les différents champs doivent suivre une structure précise pour être consommés par `openfisca` et rendus correctement par les différentes réutilisation sous forme d'applications et d'interfaces.

Toute modification est scrutée par un [validateur](https://control-center.tax-benefit.org/) qui détecte les éventuels erreurs sur les branches suivies.

Chaque commit déclenche un diagnostic qui comprend deux sections:

- un journal détaillant le déroulé du processus de validation afin de détecter des erreurs dans l'exécution de ce dernier
- un résultat de l'analyse des formats détaillants:
  - les erreurs dans l'ensemble de l'arborescence
  - les URLs erronées

Ce diagnostic est visible une fois créée ou rejoint la Merge Request (MR), trouvable dans Code/Merge_Request. Lorsqu'une erreur est détectée par le validateur, une croix rouge apparait à côté de "Pipeline" dans l'aperçu de la MR. Elle conduit à l'arborescence des tests, qui permet ensuite d'aller voir le log simplifié du test échoué. Si l'erreur n'est pas claire, il faut cliquer sur le lien disponible après "More Infos", qui conduit à l'ensemble des tâches accomplies. Il faut ensuite cliquer sur "Erreurs" pour voir de manière plus claire le fichier et les raisons causant le problème.

Le commit déclenche également la création d'une copie du rendu final sous forme de tableau. Celui-ci apparaît dans la PR en-dessous du Pipeline, dans une case "View latest app" sur laquelle il faut cliquer pour vérifier que le résultat est bien conforme au changement voulu.
