# Guide de la législation socio-fiscale française

## Loi et réglement

Ce guide est conçu à l'attention des contributeurs ajoutant de références législatives aux barèmes IPP.

Les barèmes IPP doivent renseigner, pour chaque paramètre concerné, toutes les modifications de sa valeur, avec la référence dans les textes de loi associée à chaque modification.

Un dispositif donné est régi par une partie législative (la loi), et une partie réglementaire (décrets, arrêtés, etc.). Les règles générales d’un dispositif (ex : de quels facteurs dépend le dispositif) sont inscrites dans la loi. Puis, les règles opérationnelles (quelle application de la loi ?) sont la plupart du temps décidées par décret ou arrêté. Ainsi, la plupart du temps, les formules de calcul des prélèvements ou transferts sont renseignées dans la loi, et les paramètres par décret ou arrêté.

## La source principale d'information : Légifrance

Le site [Légifrance](https://www.legifrance.gouv.fr/ ) contient l’ensemble des textes de loi, législatifs ou règlementaire, avec tous leurs historiques depuis plusieurs dizaines d’années. C’est donc le site qui permet de connaître l’ensemble des références à inscrire sur les barèmes IPP.

Concernant la partie législative, un dispositif a ses règles générales inscrites dans la loi. Par exemple, les allocations familiales sont régies par différents articles de loi, dont le premier est [l’article L521-1 du code de la sécurité sociale (CSS)](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=19EA4D92D6B2F82DA7DF807F71CF39C1.tplgfr21s_3?idArticle=LEGIARTI000029963006&cidTexte=LEGITEXT000006073189&categorieLien=id&dateTexte= ).

Un article de loi peut changer au cours du temps, et être modifié par d’autres articles : par exemple, sur le lien ci-dessus de l’article L521-1 du CSS, on voit à gauche de la page que l’article a été créé en 1985, puis modifié en 1997, en 1999, en 2002 et en 2015. Pour chaque version de l’article, il est inscrit juste en dessous du numéro de l’article (au centre de la page) les références du texte qui modifient ou créent l’article. Regarder ces modifications de la loi permet de repérer toute modification dans les règles de calcul du dispositif. Par exemple l’article L521-1 du CSS explique les principes généraux des allocations familiales (nombre minimal d’enfant, variation du montant en fonction du nombre d’enfants, et en fonction des ressources). On voit que la modulation en fonction des ressources est présente dans la version de 2015, mais pas avant, ce qui implique la présence d’une réforme.

Mais cet article ne précise pas les paramètres utilisés pour l’application de ces règles générales. En bas de la page web de l’article sont listés tout un ensemble d’autres articles qui citent cet article. Parmi eux, des textes réglementaires (commençant par D ou R), dont par exemple [l’article D521-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=20A5000F1D8695359D61A1EDA74258B8.tplgfr21s_3?idArticle=LEGIARTI000030680318&cidTexte=LEGITEXT000006073189&categorieLien=id&dateTexte= ), qui liste notamment les montants des allocations familiales pour chaque enfant à charge. De même que pour l’article L521-1, on a à gauche de la page une liste des différentes versions du texte.

Souvent, lorsque les paramètres changent, ils sont déplacés de texte. Par exemple, un paramètre de 2016 peut être présent dans un arrêté de 2016, et celui de 2015 (si différent) dans un arrêté de 2015, qui est distinct. Il est donc important de regarder si d’autres textes n’ont pas été créés.

Pour gagner du temps sur ce point, un bon point de départ est de partir du site [Service public](https://www.service-public.fr/ ) , qui décrit de manière simple les règles d’un nombre important de dispositifs (vrai surtout pour les prestations sociales, et les impôts dans une moindre mesure). Ce site est actualisé de manière continue, et contient pour chaque dispositif les textes officiels associés. Si un autre texte a été créé, il est donc beaucoup plus facile de le repérer par ce biais.

Si ce site ne contient pas ou peu d’informations sur le dispositif étudié, mieux vaut dans tous les cas commencer par une recherche web pour trouver un document décrivant de manière simplifiée le dispositif dans sa version la plus récente, avant de se lancer dans la lecture exhaustive de [Légifrance](https://www.legifrance.gouv.fr/ ) . Puis dans un second temps regarder les textes de loi.

Pour revenir à la partie législative d’un dispositif, la loi est organisée par section, qui sont découpées par dispositif. Toujours dans le cas des allocations familiales, si l’on part [l’article L521-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=19EA4D92D6B2F82DA7DF807F71CF39C1.tplgfr21s_3?idArticle=LEGIARTI000029963006&cidTexte=LEGITEXT000006073189&categorieLien=id&dateTexte=), on voit en haut de la page web une arborescence, qui indique la présence d’un chapitre dédié à ces allocations. Au faisant défiler les articles, on voit que les allocations familiales sont régies par les articles L521-1 à L521-3. Donc, si l’on veut, en plus d’actualiser des paramètres déjà existants, regarder si d’autres règles ont été créées, il faut faire défiler ces différents articles.

## Autres sources d'informations sur la législation

Voici une liste non-exhaustive mais de sources très utiles qui soit complètent Légifrance, soit sont plus accessible que cette cette dernière :

- [www.service-public.fr](https://www.service-public.fr/)

- [www.bofip.impots.gouv.fr](http://bofip.impots.gouv.fr/bofip/1-PGP.html) pour les impôts

- [www.legislation.cnav.fr](https://www.legislation.cnav.fr/Pages/baremes.aspx) pour les prestations CNAV

- [Liste des JO publiés de janvier 1992 à décembre 1997](http://admi.net/jo/sommaires/), avec le sommaire des décrets et des lois qu’ils contiennent :

- [Lois de Finances promulguées depuis 1958](http://www.legifrance.gouv.fr/affichSarde.do?reprise=true&page=1&idSarde=SARDOBJT000007104806&ordre=null&nature=null&g=ls)

## La gestion des dates

Différentes dates sont présentes au sein d’un article ou encore d'une loi. En effet, il est important de comprendre la différence entre la date d'entrée en vigueur / d’application de la loi (et non de l’article) et la date de publication de cette même loi.

Un article peut être modifié par des lois, des décrets ou des ordonnances. Ce qui nous intéresse concerne la modification faite par la loi (décret, ordonnance) sur l’article, c’est pour cela que nous nous intéressons à la date d’application et de publication **de la loi**.

### La date d'entrée en vigueur ou date d'application

Tout d'abord, la **date d'entrée en vigueur ou date d'application** de la loi correspond à la date à laquelle elle est effective. Cette date se situe soit directement sur l’article, cas le plus simple. Soit au sein du texte de loi qui modifie l’article. Dans ce deuxième cas, il faut aller à la source, là où elle a été modifiée.

Attention, la date d’entrée en vigueur aussi appelé date d’application ne correspond pas à la date en rouge positionnée en haut à droite de l'article (sauf cas rare, exemple de l'abattement don numéraire) :

![≠ Date d'entrée en vigueur](./assets/legifrance_P1.png )

La date d’application est le plus souvent établit au AAAA/01/01 si cela n'est pas le cas, il faut le justifier au sein du champ `documentation`.

#### Comment trouver la date d'application ?

Plusieurs cas sont possibles.

La date d'application peut être indiquée à plusieurs endroits.

##### Directement dans l'article du CGI

##### Au sein du texte initial de l'article de loi qui modifie l'article du CGI

Il s'agit de trouver le texte initial de l'article de loi. Voici une exemple éclairant: l'battement pour don numéraire - [Article 790 G](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000026292582/2013-01-01/) - dans sa version 2013. On sait que cet article a été modifié, ici, par la [LOI n°2012-958 du 16 août 2012 - art. 5 (V)](https://www.legifrance.gouv.fr/loda/id/LEGIARTI000026289965/2012-08-18/).

- Tout d'abord, il faut aller sur l'article et la version de cet article qui nous intéresse : [Article 790 G](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000026292582/2013-01-01/) - Version 2013
![Modifié par la LOI n°2012-958 du 16 août 2012 - art. 5 (V)](./assets/legifrance_P2.png)
- Après avoir cliqué sur « Modifié par LOI n°2012-958 du 16 août 2012 - art. 5 (V) ». Il s'agit à présent de remonter à la source, la version initiale (en haut de la page de la loi), ici de la LOI n°2012-958 du 16 août 2012 - art. 5 (V) : cliquer sur "Accéder à la version initiale", cf photo ci-dessous.![Accéder à la version initiale](./assets/legifrance_P3.png)
- On arrive alors sur la [page suivante](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000026288927 ), qui est la version initiale de la LOI n°2012-958 du 16 août 2012 ayant modifiant l'article qui nous intéresse, ici l’article 790 G du code général des impôts (CGI).
- Il faut ensuite se placer au niveau de l'[article 5](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000026289012), ce qui nous intéresse dans ce texte concerne logiquement l'article 790 G mais tout particulièrement le changement de valeur du taux de l'abattement soit le passage de 10% à 15%. Cela correspond au point 1° du G. Comme le texte nous le notifie, ce point s'applique, selon le cas, aux successions ouvertes et aux donations consenties à compter de la date de publication de la présente loi. ![Article 5](./assets/legifrance_P4.png)

Il s’agit à présent de trouver la date de publication de cet article qui est identique à sa date d’application pour notre exemple.

Comme nous venons de le voir, parfois la date d'application n'est pas donnée directement mais est identique à une autre date. Deux cas sont possibles.

La date d'application peut correspondre à différentes dates.

##### La date de création de la loi

Exemple : Assurance-vie – [Article 990.I du CGI](https://www.legifrance.gouv.fr/codes/article_lc/LEGIARTI000024430038/2011-07-31/ ) – Version 2011. Cet article est modifié par la LOI n°2011-900 du 29 juillet 2011 – art. 11.
Nous nous intéressons aux modifications relatives à l’article 990.I soit le point I. de l’article 11 de la loi. D’après la version originale de cette loi, nous pouvons voir que la date d’application du I. est appliquée « à compter de l’entrée en vigueur de la présente loi ».
![Article 11](./assets/legifrance_P5.png )
Soit exceptionnellement la date indiquée en rouge en haut de l’article 990.I – Version 2011. ![Article 990.I du CGI](./assets/legifrance_P6.png)

> La date d'application est le 2011-07-31.

##### La date de publication de la loi

Cela est alors indiqué en toute lettre dans l'article, comme dans notre exemple Abattement don numéraire.

##### Une date au sein du texte initial de la loi

Cette date n'est pas dans l'article de loi mais plutôt à l’article 1 de la loi (au début de la loi), ou parfois à la fin de la loi, en bas de page.

Exemple : Abattement don numéraire - Article 790 G - Version 2020
Cet article du CGI est modifié par LOI n° 2019-1479 du 28 décembre 2019 - art. 150. Aucune date ne se trouve sur l’article du CGI (le 790.G) en question et sur l'article de loi qui le modifie (l'art. 150).
En regardant le détail de cette loi (LOI n° 2019-1479 du 28 décembre 2019), nous pouvons voir que l’article 1 notifie cela : "À compter du 1er janvier 2020 pour les autres dispositions fiscales."
![LOI n° 2019-1479 du 28 décembre 2019 ](./assets/legifrance_P7.png)

> La date d’entrée en vigueur de la loi est le 2020-01-01.

Si toutefois, malgré différentes recherches, il vous est impossible de trouver cette date il se peut que la règle à appliquer correspond à celle de la version précédente. Le plus important est de bien tout justifier au sein du champ `documentation` afin que la personne qui vous succède comprenne ce que vous avez fait.

_NB : Parfois plusieurs versions existent pour une même date d’application, il faut prendre la version qui nous intéresse, celle qui modifie la valeur, le paramètre voulu._

#### Où écrire la date d'application ?

La date d’application est à écrire à deux reprises pour un paramètre, tout d'abord au niveau du champ `values` :

```yaml
values:
  2012-01-01:
    value: 31865
  2012-08-17:
    value: 31865
  2020-01-01:
    value: 31865
```

Mais aussi plus bas au niveau du champ `official_journal_date` suivi de la date de publication au journal officiel :

```yaml
official_journal_date:
  2012-01-01: "2011-07-30"
  2012-08-17: "2012-08-17"
  2020-01-01: "2019-12-29"
```

Voici en image, exemple de l'abattement don numéraire :

- Les dates d'application et les valeurs ![date d'application ](./assets/capturebareme_P1.png )

- Les dates d'application et de publication ainsi que la docuementation ![date d'application, de publication et documentation](./assets/capturebareme_P2.png )

#### Pour résumer

- La date d'application est différente selon la version de la loi / du décret / de l'ordonnance. Pour la trouver, il faut le plus souvent se référer au texte de loi initial. Mais elle peut tout simplement être notifiée directement sur l’article.

- Elle s'écrit au format suivant : AAAA-MM-JJ et intervient à deux reprises.

- Il ne faut pas oublier de justifier cette date au sein du champ `documentation` dès lors qu’elle n’est pas établie au 1er janvier de l'année.

### La date de publication

La date de publication  correspond à la date de parution de la loi au sein du journal officiel de la République Française (JORF).

#### Comment trouver la date de publication ?

Nous pouvons trouver la date de publication directement en haut de la page de la version initiale de la loi en question. Elle se trouvera toujours à cet endroit.

En reprenant la suite du deuxième exemple, à partir de la [version initiale de la loi](https://www.legifrance.gouv.fr/loda/id/LEGIARTI000026289965/2012-08-18/ ) ayant modifié l’article 790 G, en haut de la page nous trouvons la date de publication :

![date de publication](./assets/legifrance_P8.png )

> La date de publication de la loi est le 2012-08-17.

_Comme la date de publication de la loi est identique à la date d'entrée en vigueur dans notre exemple. Ainsi la date d'entrée en vigueur est aussi le 2012-08-12._

#### Où écrire la date de publication ?

La date de pubication se situe comme indiqué ci-dessus au niveau du champ `official_journal_date`. Elle s'écrit aussi au format suivant : AAAA-MM-JJ et intervient une seule fois au sein du paramètre. ![date d'application, de publication et documentation](./assets/capturebareme_P2.png )

#### Pour résumer

- La date de publication se trouve toujours au même endroit, au sein de la version initiale de la loi.

- Elle est présente une seule fois dans le paramètre.

_NB: La date d’application de la loi est postérieure ou identique à la date de publication de la loi._

### Les références

### Les notes
