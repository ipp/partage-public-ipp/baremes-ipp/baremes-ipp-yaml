#! /usr/bin/env python

"""Barèmes IPP documentation."""


from setuptools import setup, find_packages


classifiers = """\
Development Status :: 2 - Pre-Alpha
License :: OSI Approved :: GNU Affero General Public License v3
Operating System :: POSIX
Programming Language :: Python
Topic :: Scientific/Engineering :: Information Analysis
"""

doc_lines = __doc__.split('\n')


setup(
    name = 'baremes-ipp-doc',
    version = '0.1.0',
    author = 'IPP',
    author_email = 'mahdi.benjelloul@ipp.eu',
    classifiers = [classifier for classifier in classifiers.split('\n') if classifier],
    description = doc_lines[0],
    keywords = 'tax benefits',
    license = 'http://www.etalab.gouv.fr/licence-ouverte-open-licence',
    long_description = '\n'.join(doc_lines[2:]),
    url = 'https://gitlab.com/ipp/baremes-ipp/baremes-ipp-yaml',
    entry_points = {
        'console_scripts': []
        },
    extras_require = dict(
        doc = [
            'mkdocs-material',
            'mkdocs-git-revision-date-localized-plugin',
            ]
        ),
    install_requires = [],
    packages = find_packages(),
    )
