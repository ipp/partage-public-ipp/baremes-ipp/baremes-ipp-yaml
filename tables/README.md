# Configuration des tables de paramètres

Les fichiers situés dans ce répertoire définissent la structure de la version Web des paramètres IPP.

Chaque fichier représente une section des paramètres sur le site de l'IPP [`https://www.ipp.eu/`](https://www.ipp.eu/)].

Voir la [documentation du format des fichiers](https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-views/blob/master/config-doc.md).
